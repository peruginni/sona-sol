
var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

gulp.task('less', function () {
  return gulp.src('./src/main/resources/assets/stylesheets/main.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./src/main/resources/static/stylesheets'));
});

gulp.task('default', function() {
    // place code for your default task here
});
