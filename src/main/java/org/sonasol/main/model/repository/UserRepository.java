package org.sonasol.main.model.repository;

import org.sonasol.main.model.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long>
{
    public User findByEmail(String email);
}
