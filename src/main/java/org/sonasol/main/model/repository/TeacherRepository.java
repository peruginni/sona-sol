package org.sonasol.main.model.repository;

import org.sonasol.main.model.entity.Location;
import org.sonasol.main.model.entity.Teacher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Long>
{

}
