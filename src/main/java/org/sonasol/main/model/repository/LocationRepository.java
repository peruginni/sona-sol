package org.sonasol.main.model.repository;

import org.sonasol.main.model.entity.Lesson;
import org.sonasol.main.model.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long>
{

}
