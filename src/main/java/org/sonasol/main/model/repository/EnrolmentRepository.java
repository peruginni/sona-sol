package org.sonasol.main.model.repository;

import org.sonasol.main.model.entity.Enrolment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnrolmentRepository extends CrudRepository<Enrolment, Long>
{

}
