package org.sonasol.main.model.entity;

import javax.persistence.*;

@Entity
public class Location
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String title;

    @Column
    private String slug;

    @Column
    private String description;

    @Column
    private String mapLink;

    @Column
    private String mapImageLink;

    @Column
    private String transportLink;

    @Column
    private String address;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @Column
    private String navigationInstruction;

}
