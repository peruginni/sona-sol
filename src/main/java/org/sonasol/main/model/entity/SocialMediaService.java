package org.sonasol.main.model.entity;

public enum SocialMediaService
{
    FACEBOOK,
    GOOGLE,
}
