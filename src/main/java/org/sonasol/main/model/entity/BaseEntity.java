package org.sonasol.main.model.entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity<ID>
{
    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdAt;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    protected Date modifiedAt;

    @Version
    private long version;

    public Date getCreatedAt()
    {
        return createdAt;
    }

    public Date getModifiedAt()
    {
        return modifiedAt;
    }

    public long getVersion()
    {
        return version;
    }

    @PrePersist
    public void prePersist()
    {
        Date now = new Date();
        this.createdAt = now;
        this.modifiedAt = now;
    }

    @PreUpdate
    public void preUpdate()
    {
        this.modifiedAt = new Date();
    }
}
