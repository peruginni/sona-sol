package org.sonasol.main.model.entity;

public enum Role
{
    ROLE_USER,
    ROLE_TEACHER,
    ROLE_ADMIN
}
