package org.sonasol.main.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Teacher
{
    enum Sex {
        MALE,
        FEMALE
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String slug;

    @Column
    private String summary;

    @Column
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @Column
    private Long userId;

    @Column(length = 20000)
    private String certificates;

}
