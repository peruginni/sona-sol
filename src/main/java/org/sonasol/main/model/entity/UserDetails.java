package org.sonasol.main.model.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.social.security.SocialUser;

import javax.persistence.Entity;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UserDetails extends SocialUser
{
    private Long id;

    private String name;

    private String lastName;

    private Role role;

    private SocialMediaService socialSignInProvider;

    public UserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities)
    {
        super(username, password, authorities);
    }
}
