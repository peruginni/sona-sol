package org.sonasol.main.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
public class Enrolment implements Serializable
{
    protected static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column()
    protected String email;

    @Column()
    protected String password;

    @Column()
    protected String name;

    @Column()
    protected String phone;

    @Column()
    protected String address;

    @Column()
    @Temporal(TemporalType.DATE)
    protected Date birthday;

    @Column(length = 20000)
    protected String note;

    @Column()
    protected String favouriteSong;

    @Column()
    protected String ipAddress;

    @Column()
    protected String browser;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdAt;

    @OneToMany()
    protected Set<Lesson> lessons;

    @Column()
    protected Double price;

    @Column()
    protected String priceLevel;

}
