package org.sonasol.main.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
public class Lesson implements Serializable
{
    enum Level {
        TotalBeginners,
        Beginners,
        Primary,
        Intermediate,
        Open
    }

    enum Tag {
        SoftShoes,
        HardShoes,
        SetDance,
        CVUT,
        U3V,
        Choreo
    }

    enum Day {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Sunday,
        Saturday
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String title;

    @Column
    private String slug;

    @Column(length = 20000)
    private String description;

    @Column
    @Enumerated(EnumType.STRING)
    private Level level;

    @ElementCollection(targetClass = Tag.class)
    @CollectionTable(name = "lesson_tag",
            joinColumns = @JoinColumn(name = "tag_id"))
    @Column
    private Set<Tag> tags;

    @Column
    private Double basePrice;

    @Column
    private Day day;

    @Column
    private String timeStart;

    @Column
    private String timeEnd;

    @Column
    @Temporal(TemporalType.DATE)
    private Date availableSince;

    @Column
    @Temporal(TemporalType.DATE)
    private Date availableUntil;

    @Column
    private Integer capacity;

    @OneToMany()
    private Set<Teacher> teachers;

    @ManyToOne
    private Location location;


}
