package org.sonasol.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MediaController
{
    @RequestMapping("/galerie")
    public ModelAndView gallery(ModelMap model)
    {
        return new ModelAndView("media/gallery");
    }

    @RequestMapping("/album")
    public ModelAndView album(ModelMap model)
    {
        return new ModelAndView("media/album");
    }

}
