package org.sonasol.main.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AboutController
{
    @RequestMapping("/kdo-jsme")
    public ModelAndView whoWeAre(ModelMap model)
    {
        return new ModelAndView("about/whoWeAre");
    }

    @RequestMapping("/vystoupeni")
    public ModelAndView performances(ModelMap model)
    {
        return new ModelAndView("about/performances");
    }

    @RequestMapping("/nabidka-vystoupeni")
    public ModelAndView performanceOffer(ModelMap model)
    {
        return new ModelAndView("about/performanceOffer");
    }

    @RequestMapping("/kontakt")
    public ModelAndView contact(ModelMap model)
    {
        return new ModelAndView("about/contact");
    }
}
