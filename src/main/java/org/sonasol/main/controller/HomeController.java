package org.sonasol.main.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController
{
    @Value("${name}")
    private String name;

    @RequestMapping("/")
    public ModelAndView welcome (ModelMap model)
    {
        model.addAttribute("name", this.name);
        return new ModelAndView("home/welcome");
    }
}
