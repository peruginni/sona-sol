package org.sonasol.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController
{
    @RequestMapping("/prihlasit")
    public ModelAndView login(ModelMap model)
    {
        return new ModelAndView("user/login");
    }

    @RequestMapping("/odhlasit")
    public ModelAndView logout(ModelMap model)
    {
        return new ModelAndView("user/logout");
    }

    @RequestMapping("/profil")
    public ModelAndView profil(ModelMap model)
    {
        return new ModelAndView("user/profil");
    }
}
