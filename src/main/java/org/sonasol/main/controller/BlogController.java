package org.sonasol.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlogController
{
    @RequestMapping("/blog")
    public ModelAndView list(ModelMap model)
    {
        return new ModelAndView("blog/list");
    }

    @RequestMapping("/blog/detail")
    public ModelAndView detail(ModelMap model)
    {
        return new ModelAndView("blog/detail");
    }

}
