package org.sonasol.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IrishDanceController
{
    @RequestMapping("/irske-tance")
    public ModelAndView about(ModelMap model)
    {
        return new ModelAndView("irishDance/about");
    }

    @RequestMapping("/boty")
    public ModelAndView shoes(ModelMap model)
    {
        return new ModelAndView("irishDance/shoes");
    }
}
