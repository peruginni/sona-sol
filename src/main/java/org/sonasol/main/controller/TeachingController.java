package org.sonasol.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TeachingController
{
    @RequestMapping("/vyuka")
    public ModelAndView lessons(ModelMap model)
    {
        return new ModelAndView("teaching/lessons");
    }

    @RequestMapping("/prihlaska")
    public ModelAndView enrollment(ModelMap model)
    {
        return new ModelAndView("teaching/enrollment");
    }

    @RequestMapping("/cenik")
    public ModelAndView pricing(ModelMap model)
    {
        return new ModelAndView("teaching/pricing");
    }

    @RequestMapping("/rozvrh")
    public ModelAndView schedule(ModelMap model)
    {
        return new ModelAndView("teaching/schedule");
    }
}
